FROM python:3.9.0-alpine

WORKDIR /usr/src/fibonacci

COPY ./requirements.txt /usr/src/fibonacci/requirements.txt

RUN pip install -r requirements.txt

COPY . /usr/src/app
