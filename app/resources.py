import json

from celery.result import AsyncResult
import falcon

from app.fibonacci import fibonacci


class FibonacciResource:
    """
        Initial resource, get limit parameter
        and start the fibonacci task

        Returns:
            task_id
    """
    def on_get(self, req, resp, limit):
        resp.status = falcon.HTTP_200
        task = fibonacci.delay(limit)
        resp.body = json.dumps({
            'status': 'success',
            'task_id': task.id
        })


class FibonacciStatus:
    """
        Resource to check process status and get response
    """
    def on_get(self, req, resp, task_id):
        task_result = AsyncResult(task_id)
        result = {
            'status': task_result.status
        }
        if task_result.status == 'SUCCESS':
            result['result'] = task_result.result
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(result)


class HealthcheckResource:
    """
        Resource for check service health
    """
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.body = '{}'
