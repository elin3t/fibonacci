from unittest import TestCase
from unittest.mock import patch

from app.fibonacci import (
    fibonacci,
    fibonnaci_generator
)


class TestFibonacciList(TestCase):
    def test_fibonacci_list(self):
        fib_ret = ['0', '1', '1', '2', '3', '5', '8', '13', '21', '34', '55']
        fib_list = fibonnaci_generator(10)
        self.assertEqual(fib_list, fib_ret)
        self.assertEqual(len(fib_list), 11)

    @patch('app.fibonacci.fibonacci')
    def test_fibonacci_task(self, mock_fib):
        fib_error = 'Error number should be greater or iqual than zero'
        mock_fib.run.return_value = fib_error
        self.assertEqual(fibonacci.run(-1), fib_error)
        mock_fib.run.return_value = ['0']
        self.assertEqual(fibonacci(0), "['0']")
        _ = ['0', '1', '1', '2', '3', '5', '8', '13', '21', '34', '55']
        mock_fib.run.return_value = _
        _ = "['0', '1', '1', '2', '3', '5', '8', '13', '21', '34', '55']"
        self.assertEqual(fibonacci(10), _)
