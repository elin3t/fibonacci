from falcon import testing
import pytest

from app.app import app


@pytest.fixture()
def client():
    return testing.TestClient(app)


def test_get_healthcheck(client):
    response = {}
    result = client.simulate_get('/healthcheck')
    assert result.json == response
