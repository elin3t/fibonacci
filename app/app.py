import falcon

from app.config import cache
from app.resources import (
    FibonacciResource,
    FibonacciStatus,
    HealthcheckResource
)


app = falcon.API(middleware=cache.middleware)
fibonacci = FibonacciResource()
fibonacci_result = FibonacciStatus()
healthcheck = HealthcheckResource()

app.add_route('/healthcheck', healthcheck)
app.add_route('/fibonacci/{limit:int}', fibonacci)
app.add_route('/fibonacci/status/{task_id}', fibonacci_result)
