import os

from dotenv import load_dotenv
from falcon_caching import Cache


load_dotenv()

cache = Cache(
    config={
        'CACHE_EVICTION_STRATEGY': os.getenv('CACHE_STRATEGY', 'time-based'),
        'CACHE_TYPE': os.getenv('CACHE_TYPE', 'simple')
    })
