import os

import celery
from dotenv import load_dotenv

load_dotenv()


CELERY_BROKER = os.getenv('CELERY_BROKER')
CELERY_BACKEND = os.getenv('CELERY_BACKEND')

app = celery.Celery(
    'tasks',
    broker=CELERY_BROKER,
    backend=CELERY_BACKEND,
    include=['app.fibonacci']
)
