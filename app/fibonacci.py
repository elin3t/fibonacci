from app.config import cache
from app.tasks import app


@app.task
def fibonacci(limit):
    """
        retrieve the list of fibonacci numbers
        get it from cache, or save the new list on cache
        after use the generator for get it
    """
    if limit < 0:
        return 'Error number should be greater or iqual than zero'
    fib_key = 'fibonacci_list'
    fib_max = cache.get('fib_max') or 0
    if fib_max >= limit:
        fib_list = cache.get(fib_key) or ['0']
        return str(fib_list[:limit+1])
    else:
        fib_max = limit
        cache.set('fib_max', limit)
        fib_list = cache.get(fib_key) if cache.has(fib_key) else ['0', '1']
        new_fib_list = fibonnaci_generator(
            limit,
            fib_list
        )
        cache.set(fib_key, new_fib_list)
    return str(new_fib_list)


def fibonnaci_generator(limit: int, fib: list = ['0', '1']):
    """ return fibonacci list from fib list len to limit """
    if limit == 1:
        return ['0', '1']
    for _ in range(len(fib), limit+1):
        fib.append(str(int(fib[-1])+int(fib[-2])))
    return fib
