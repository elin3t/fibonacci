# Fibonacci List Generator

* This is an fibonacci list generator service
* Takes a number N from url and return first N fibonacci numbers
* Numbers on list are written as strings, in order to be able to manage large numbers without convert them to float format

## Stack

* python3
* Falcon
* Celery
* Redis

## Endpoints
* /healthcheck --> for service helth checking
* /fibonacci/{n} --> create a task for creating the first n fibonacci numbers
* /fibonacci/status/{task_id} --> return the status and if it is success returns the result of the request
## How to run it

### Locally
* Create a virtual env
* Install the requirements
* Setup the env vars
* Run Redis, Celery and Project

### Docker
* build it and run 
    ```shell
        docker-compose up -d --build
    ```
* visit 0.0.0.0:8000/healthcheck or any other endpoint 

### Running the tests



## TODO's
* unit tests
* Make the response a donwloable file